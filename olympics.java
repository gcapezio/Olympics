package olympics;
import java.util.ArrayList;

public class Olympics {
private String city;
private int year;
private ArrayList<OlympicEvent> events;
private static int numberofOlympicGames= 40;

public Olympics(String newCity, int newYear){
	city=newCity;
	year=newYear;
	events= new ArrayList<OlympicEvent>();
}
public String getCity(){
	return city;
}
public int getYear(){
	return year;
}
public void addEvent(OlympicEvent e){
	events.add(e);
}
public OlympicEvent getEvent(int i){
	return events.get(i);
	
}
public static int getnummberofOlympicGames(){
	return numberofOlympicGames;
}
}
